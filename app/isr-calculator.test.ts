import { expect, it } from 'vitest';
import { calculateISR } from './isr-calculator';

it.each([18000*12, 19919.95*12])('should %f show zero when salary is below treshold', (salary) => {
    const tax = calculateISR(salary)
    expect(tax).toBe(0);
});

it('should show 144.08 of tax as shown in example with 20,000 monthly salary', () => {
    const tax = calculateISR(20000*12);
    expect(tax).toBe(144.08)
})
