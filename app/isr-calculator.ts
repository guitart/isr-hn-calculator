export const calculateISR = (salary: number): number => {
    const exento = 199039.47
    const deducible = 40000;
    let taxable = salary - deducible;
    if (taxable <= exento) {
        return 0;
    }
    taxable -= exento;
    return Math.round(taxable*0.15*100)/100;
}